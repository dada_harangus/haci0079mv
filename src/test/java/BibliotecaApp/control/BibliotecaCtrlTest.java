package BibliotecaApp.control;

import BibliotecaApp.model.Carte;
import BibliotecaApp.model.repo.CartiRepo;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

@Before
public void setUp(){




}



    @Test (expected = Exception.class)  //editura prea scurta
    public void adaugaCarte() throws Exception {
       CartiRepo CR=new CartiRepo();
       BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
       Carte carte =new Carte();
       carte.setTitlu("Poezii");
       carte.setEditura("Corint");
       carte.setAnAparitie("2018");

            BibliotecaCtrl.adaugaCarte(carte);

}


    @Test
    public void adaugaCarte1() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("Abracadabra");
        carte.setEditura("Renasterea");
        carte.setAnAparitie("1888");

        BibliotecaCtrl.adaugaCarte(carte);

    }

    @Test (expected = Exception.class)//an prea mic
    public void adaugaCarte2() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("Harry Potter");
        carte.setEditura("J.K.Rowling");
        carte.setAnAparitie("1772");

        BibliotecaCtrl.adaugaCarte(carte);

    }

    @Test (expected = Exception.class) //an prea mare
    public void adaugaCarte3() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("Abracadabra");
        carte.setEditura("Renasterea");
        carte.setAnAparitie("2019");

        BibliotecaCtrl.adaugaCarte(carte);

    }


    @Test (expected = Exception.class)//titlu prea mic
    public void adaugaCarte4() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("");
        carte.setEditura("Renasterea");
        carte.setAnAparitie("1888");

        BibliotecaCtrl.adaugaCarte(carte);

    }

    @Test //titlu o litera
    public void adaugaCarte5() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("T");
        carte.setEditura("Renasterea");
        carte.setAnAparitie("1888");

        BibliotecaCtrl.adaugaCarte(carte);

    }


    @Test
    public void adaugaCarte6() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("E.T");
        carte.setEditura("Renasterea");
        carte.setAnAparitie("1801");

        BibliotecaCtrl.adaugaCarte(carte);

    }

    @Test
    public void adaugaCarte7() throws Exception {
        CartiRepo CR=new CartiRepo();
        BibliotecaCtrl BibliotecaCtrl =new BibliotecaCtrl(CR);
        Carte carte =new Carte();

        carte.setTitlu("Basme");
        carte.setEditura("Presa universitara");
        carte.setAnAparitie("1923");

        BibliotecaCtrl.adaugaCarte(carte);

    }





}





